var apoteka = angular.module("apotekaModule", ['ngRoute']);

apoteka.controller("lekoviCtrl", function($scope, $http, $location){
	
	
	var baseUrl = "/api/apoteke";
	
	$scope.lekovi = [];
	$scope.apoteke = [];
	$scope.proizvodjaci = [];
	
	$scope.noviLek = {};
	$scope.noviLek.naziv = "";
	$scope.noviLek.generickiNaziv = "";
	$scope.noviLek.kolicina = "";
	$scope.noviLek.cena = "";
	$scope.noviLek.apotekaId = "";
	$scope.noviLek.apotekaNaziv = "";
	$scope.noviLek.proizvodjacId = "";
	$scope.noviLek.proizvodjacNaziv = "";

	
	$scope.trazeniLek = {};
	$scope.trazeniLek.naziv = "";
	$scope.trazeniLek.generickiNaziv = "";
	$scope.trazeniLek.apotekaId = "";
	
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	var getLekovi = function(id){
		
		$scope.trazeniLek.generickiNaziv = $scope.trazeniLek.naziv;
		
		var config = { params: {}};
		
		if($scope.trazeniLek.naziv != ""){
			config.params.naziv = $scope.trazeniLek.naziv;
		}
		if($scope.trazeniLek.generickiNaziv != ""){
			config.params.generickiNaziv = $scope.trazeniLek.generickiNaziv;
		}
		if($scope.trazeniLek.apotekaId != ""){
			config.params.apotekaId = $scope.trazeniLek.apotekaId ;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(baseUrl + "/" + id + "/lekovi",  config);
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.lekovi = data.data;
					$scope.trazeniLek = {};
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	
	
	var getApoteke = function(){
		
		var promise = $http.get("/api/apoteke");
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.apoteke = data.data;
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getApoteke();

	
	var getProizvodjaci = function(){
		
		var promise = $http.get("/api/proizvodjaci");
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.proizvodjaci = data.data;
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getProizvodjaci();
	
	$scope.addLek = function(apotekaId){
		
		
		$http.post(baseUrl + "/" + apotekaId + "/lekovi", $scope.noviLek).then(
			function success(data){
				getLekovi(apotekaId);
				$scope.noviLek = {};
			},
			function error(data){
				alert("Neuspesno dodavanje leka.");
			}
		)
	}
	
	
	$scope.kupovina = function(apotekaId, lekId){
		$location.path("/kupovina/apoteke/" + apotekaId + "/lekovi/" + lekId);
	}
	
	$scope.izmena = function(lekId, apotekaId){
		$location.path("/lekovi/edit/apoteke/" + apotekaId + "/lekovi/" + lekId );
	}
	
	$scope.page = function(direction){
		$scope.pageNum = $scope.pageNum + direction;
		getLekovi();
	}
	
	
	$scope.filtriranje = function(apotekaId){
		$scope.pageNum = 0;
		getLekovi(apotekaId);
	}
	
	
	$scope.ponistavanje = function(){
		$scope.pageNum = 0;
		$scope.trazeniLek = {};
	}
	
 
	
	$scope.deleteLek= function(apotekaId, lekId){
		
		var promise = $http.delete(baseUrl + "/" + apotekaId + "/lekovi/" + lekId);
		promise.then(
			function uspeh(data){
				getLekovi(apotekaId);
			},
			function neuspeh(data){
				alert("Nije uspelo brisanje leka!");
			}
		);
	}
	
});

apoteka.controller("editLekaCtrl", function($scope, $http, $routeParams, $location){
	
	var apotekaId = $routeParams.aid;
	var lekId = $routeParams.lid;
	var baseUrl = "/api/apoteke";
	
	$scope.apoteke = [];
	$scope.proizvodjaci = [];
	
	$scope.stariLek = {};
	$scope.stariLek.apotekaId = "";
	$scope.stariLek.id = "";
	
	var getStariLek = function(){
		
		var promise = $http.get(baseUrl + "/" + apotekaId + "/lekovi/" + lekId);
		promise.then(
			function success(obj){
				$scope.stariLek = obj.data;
			},
			function error(obj){
				alert("Neuspesno dobavljanje leka!");
			}
		);
	}
	
	getStariLek();
	
	var getApoteke = function(){
		
		var promise = $http.get("/api/apoteke");
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.apoteke = data.data;
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getApoteke();
	

	var getProizvodjaci = function(){
		
		var promise = $http.get("/api/proizvodjaci");
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.proizvodjaci = data.data;
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getProizvodjaci();
	
	 
	
	$scope.editStariLek = function () {
		$http.put(baseUrl + "/" + $scope.stariLek.apotekaId + "/lekovi/" + $scope.stariLek.id, $scope.stariLek).then(
			function success(data){
				$location.path("/lekovi");
			},
			function error(data){
				alert("Neuspela izmena lekovi.");
			}
		)};		

})


apoteka.controller("kupovinaLekaCtrl", function($scope, $http, $routeParams, $location){
	
	var apotekaId = $routeParams.aid;
	var lekId = $routeParams.lid;
	var baseUrl = "/api/apoteke";
	
	$scope.apoteke = [];
	$scope.proizvodjaci = [];
	
	$scope.stariLek = {};
	$scope.stariLek.apotekaId = "";
	$scope.stariLek.id = "";
	
	var getStariLek = function(){
		
		var promise = $http.get(baseUrl + "/" + apotekaId + "/lekovi/" + lekId);
		promise.then(
			function success(obj){
				$scope.stariLek = obj.data;
			},
			function error(obj){
				alert("Neuspesno dobavljanje leka!");
			}
		);
	}
	
	getStariLek();
	
	
	$scope.kupovina = {};
	$scope.kupovina.kolicina = "";
	$scope.kupovina.lekId = "";
	
	$scope.kupiLek = function (id){
		
		if($scope.kupovina.kolicina == ""){
			alert("unesite zeljenu kolicinu!")
			return;
		}
		
		$scope.kupovina.lekId = id;
		
		
		$http.post("/api/kupovine", $scope.kupovina).then(
				function success(data){
					var kup = data.data;
					
					alert("Uspesno ste kupili lek \""
						+ kup.lekNaziv +  "\",\n Komada: " + kup.kolicina +
						"\n Ukupna cena: " +
						+ kup.ukupnaCena + " dinara!" );
					
					$scope.kupovina = {};
					$location.path("/lekovi");
					
					
				},
				function error(data){
					alert("Kupovina leka nije uspela.");
					$scope.kupovina = {};
					$location.path("/lekovi");
				}
			)
	}

})
	


apoteka.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : '/app/html/partial/home.html'
		})
		.when('/lekovi', {
			templateUrl : '/app/html/partial/lekovi.html'
		})
		.when('/lekovi/edit/apoteke/:aid/lekovi/:lid', { 
			templateUrl : '/app/html/partial/edit_lek.html'
		})
		.when('/kupovina/apoteke/:aid/lekovi/:lid', { 
			templateUrl : '/app/html/partial/kupovina.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);

