package it.apoteka.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Apoteka {

	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Long id;
	
	@Column(nullable=false,unique=true)
	private String naziv;
	
	@Column
	private String adresa;
	
	@OneToMany(mappedBy="apoteka", cascade=CascadeType.REMOVE)
	private List<Lek> lekovi = new ArrayList<>();
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public List<Lek> getLekovi() {
		return lekovi;
	}
	public void setLekovi(List<Lek> lekovi) {
		this.lekovi = lekovi;
	}
 
	
	
	
	
	
	
	
}
