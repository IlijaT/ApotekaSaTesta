package it.apoteka.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Lek {
	
	
	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	@Column(nullable=false,unique=true)
	private String naziv;
	
	@Column(nullable=false)
	private String generickiNaziv;
	
	@Column
	private Integer kolicina;
	@Column
	private Double cena;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Apoteka apoteka;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Proizvodjac proizvodjac;
	
	@OneToMany(mappedBy="lek",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<Kupovina> kupovine = new ArrayList<>();
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getGenerickiNaziv() {
		return generickiNaziv;
	}
	public void setGenerickiNaziv(String generickiNaziv) {
		this.generickiNaziv = generickiNaziv;
	}
	public Integer getKolicina() {
		return kolicina;
	}
	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}
	public Double getCena() {
		return cena;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	public Apoteka getApoteka() {
		return apoteka;
	}
	public void setApoteka(Apoteka apoteka) {
		this.apoteka = apoteka;
	}
	public Proizvodjac getProizvodjac() {
		return proizvodjac;
	}
	public void setProizvodjac(Proizvodjac proizvodjac) {
		this.proizvodjac = proizvodjac;
	}
	
	
	
	
	 
}
