package it.apoteka.web.dto;

public class KupovinaDTO {
	
	private Long id;
	 
	private Long lekId;
	
	private String lekNaziv;
 
	private Integer kolicina;
	
	private Double ukupnaCena;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLekId() {
		return lekId;
	}

	public void setLekId(Long lekId) {
		this.lekId = lekId;
	}

	public String getLekNaziv() {
		return lekNaziv;
	}

	public void setLekNaziv(String lekNaziv) {
		this.lekNaziv = lekNaziv;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Double getUkupnaCena() {
		return ukupnaCena;
	}

	public void setUkupnaCena(Double ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}
	
	

}
