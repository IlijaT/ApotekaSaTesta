package it.apoteka.web.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.apoteka.model.Proizvodjac;
import it.apoteka.service.ProizvodjacService;
import it.apoteka.support.ProizvodjacToProizvodjacDTO;
import it.apoteka.web.dto.ProizvodjacDTO;


@RestController
@RequestMapping(value="/api/proizvodjaci")
public class ApiProizvodjacController {

	@Autowired
	private ProizvodjacService proizvodjacService;
	
	 

	@Autowired
	private ProizvodjacToProizvodjacDTO toDTO;
	

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ProizvodjacDTO>> get( 
			@RequestParam(defaultValue = "0") int pageNum
			) {
		
		Page<Proizvodjac> proizvodjaci = proizvodjacService.findAll(pageNum);

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(proizvodjaci.getTotalPages()) );
		
		return new ResponseEntity<>(toDTO.convert(proizvodjaci.getContent()), headers, HttpStatus.OK);
	}

}
