package it.apoteka.web.controler;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.apoteka.model.Lek;
import it.apoteka.service.LekService;
import it.apoteka.support.LekDTOToLek;
import it.apoteka.support.LekToLekDTO;
import it.apoteka.web.dto.LekDTO;
 

@RestController
@RequestMapping("/api/apoteke")
public class ApiLekController {

	@Autowired
	private LekService lekService;
//	@Autowired
//	private ApotekaService apotekaService;

	@Autowired
	private LekDTOToLek toLek;

	@Autowired
	private LekToLekDTO toDTO;

	@RequestMapping(value = "/{id_apoteke}/lekovi", method = RequestMethod.GET)
	public ResponseEntity<List<LekDTO>> get(
			@PathVariable Long id_apoteke,
			@RequestParam(required = false) String naziv,
			@RequestParam(required = false) String generickiNaziv, 
			@RequestParam(required = false) Long apotekaId,
			@RequestParam(defaultValue = "0") int pageNum) {
		
		if (id_apoteke == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Page<Lek> lekovi;

		if (naziv != null || generickiNaziv != null || apotekaId != null) {
			lekovi = lekService.pretraga(naziv, generickiNaziv, apotekaId, pageNum);
//			lekovi = lekService.pretraga(naziv, apotekaId, pageNum);
		} else {
			lekovi = lekService.findByApotekaId(id_apoteke, pageNum);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(lekovi.getTotalPages()) );
		
		return new ResponseEntity<>(toDTO.convert(lekovi.getContent()), headers, HttpStatus.OK);
	}

	
	
	@RequestMapping(value = "/{id_apoteke}/lekovi/{id}", method = RequestMethod.GET)
	public ResponseEntity<LekDTO> get(@PathVariable Long id_apoteke, @PathVariable Long id, @RequestParam(defaultValue = "0") int pageNum ) {

		Page<Lek> lek = lekService.findByApotekaId(id_apoteke, pageNum);

		if (lek == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		List<Lek> lekovi = lek.getContent();
		
		Lek trazeniLek = null;
		
		for (Lek l : lekovi) {
			if( l.getId() == id) {
				
				trazeniLek = l;
			}
		}
		

		return new ResponseEntity<>(toDTO.convert(trazeniLek), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id_apoteke}/lekovi", method = RequestMethod.POST)
	public ResponseEntity<LekDTO> add(@PathVariable Long id_apoteke, @RequestBody @Validated LekDTO newLek) {

		Lek lek = toLek.convert(newLek);

		Lek persisted = lekService.save(lek);

		return new ResponseEntity<>(toDTO.convert(persisted), HttpStatus.CREATED);
	}

	
	
	@RequestMapping(value = "/{id_apoteke}/lekovi/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<LekDTO> edit(@PathVariable Long id, @PathVariable Long id_apoteke, @RequestBody LekDTO editedLek) {

		if (id_apoteke == null || id == null || !id.equals(editedLek.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Lek lek = toLek.convert(editedLek);

		Lek persisted = lekService.save(lek);

		return new ResponseEntity<>(toDTO.convert(persisted), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id_apoteke}/lekovi/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<LekDTO> delete(@PathVariable Long id, @PathVariable Long id_apoteke) {
		
		if (id == null || id_apoteke == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		lekService.delete(id);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
