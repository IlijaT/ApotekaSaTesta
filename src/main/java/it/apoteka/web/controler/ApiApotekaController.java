package it.apoteka.web.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.apoteka.model.Apoteka;
import it.apoteka.service.ApotekaService;
import it.apoteka.support.ApotekaToApotekaDTO;
import it.apoteka.web.dto.ApotekaDTO;



@RestController
@RequestMapping("/api/apoteke")
public class ApiApotekaController {
	
	@Autowired
	private ApotekaService apotekaService;
	@Autowired
	private ApotekaToApotekaDTO toDTO;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ApotekaDTO>> get(@RequestParam(defaultValue="0") int pageNum){
		
		Page<Apoteka> apoteke= apotekaService.findAll(pageNum);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(apoteke.getTotalPages()) );
		return  new ResponseEntity<>(
				toDTO.convert(apoteke.getContent()),
				headers,
				HttpStatus.OK);
	}
	
}
