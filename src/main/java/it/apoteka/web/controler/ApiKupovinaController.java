package it.apoteka.web.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.apoteka.model.Kupovina;
import it.apoteka.service.KupovinaService;
import it.apoteka.support.KupovinaToKupovinaDTO;
import it.apoteka.web.dto.KupovinaDTO;


@RestController
@RequestMapping(value= "/api/kupovine")
public class ApiKupovinaController {
	
	@Autowired
	private KupovinaService kupovinaService;
	
	@Autowired
	private KupovinaToKupovinaDTO toDTO;
	
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<KupovinaDTO> add(@RequestBody KupovinaDTO newKupovina) {

		Kupovina kupovina = kupovinaService.kupi(newKupovina.getLekId(), newKupovina.getKolicina()); 
		
		if(kupovina == null)
			return new ResponseEntity<>(HttpStatus.CONFLICT);

		return new ResponseEntity<>(toDTO.convert(kupovina), HttpStatus.CREATED);
	}

}
