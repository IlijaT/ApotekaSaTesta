package it.apoteka.service.impl;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.apoteka.model.Proizvodjac;
import it.apoteka.repository.ProizvodjacRepository;
import it.apoteka.service.ProizvodjacService;


@Service
@Transactional
public class JpaProizvodjacServiceImpl implements ProizvodjacService {
	
	@Autowired
	private ProizvodjacRepository proizvodjacRepository;

	@Override
	public Page<Proizvodjac> findAll(int pageNum) {
		return proizvodjacRepository.findAll(new PageRequest(pageNum, 10));
	}

	@Override
	public Proizvodjac findOne(Long id) {
		return proizvodjacRepository.findOne(id);
	}

	@Override
	public Proizvodjac save(Proizvodjac proizvodjac) {
		return proizvodjacRepository.save(proizvodjac);
	}

	@Override
	public void delete(Long id) {
		proizvodjacRepository.delete(id);
		
	}
	
	 

}
