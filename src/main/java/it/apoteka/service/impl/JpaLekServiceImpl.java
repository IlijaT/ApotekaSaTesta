package it.apoteka.service.impl;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.apoteka.model.Lek;
import it.apoteka.repository.LekRepository;
import it.apoteka.service.LekService;


@Service
@Transactional
public class JpaLekServiceImpl implements LekService {
	
	@Autowired
	private LekRepository lekRepository;

	@Override
	public Lek findOne(Long id) {
		return lekRepository.findOne(id);
	}

	@Override
	public Page<Lek> findAll(int pageNum) {
		return lekRepository.findAll(new PageRequest(pageNum, 10));
	}

	@Override
	public Lek save(Lek lek) {
		return lekRepository.save(lek);
	}

	@Override
	public void delete(Long id) {
		lekRepository.delete(id);

	}

	@Override
	public Page<Lek> pretraga(String naziv, String generickiNaziv, Long apotekaId, int page) {
		return lekRepository.pretraga(naziv, generickiNaziv, apotekaId, new PageRequest(page, 10));
	}
	
//	@Override
//	public Page<Lek> pretraga(String naziv, Long apotekaId, int page) {
//		return lekRepository.pretraga(naziv, apotekaId, new PageRequest(page, 10));
//	}

	@Override
	public Page<Lek> findByApotekaId(Long apotekaId, int page) {
		return lekRepository.findByApotekaId(apotekaId, new PageRequest(page, 10));
	}

	 
	 

	 

	 
 

}
