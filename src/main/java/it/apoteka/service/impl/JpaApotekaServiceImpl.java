package it.apoteka.service.impl;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.apoteka.model.Apoteka;
import it.apoteka.repository.ApotekaRepository;
import it.apoteka.service.ApotekaService;

@Service
@Transactional
public class JpaApotekaServiceImpl implements ApotekaService {

	
	@Autowired
	private ApotekaRepository apotekaRepository;
 

	@Override
	public Apoteka findOne(Long id) {
		return apotekaRepository.findOne(id);
	}

	@Override
	public Page<Apoteka> findAll(int pageNum) {
		return apotekaRepository.findAll(new PageRequest(pageNum, 10));
	}


	@Override
	public void delete(Long id) {
		apotekaRepository.delete(id);

	}

	@Override
	public Apoteka save(Apoteka apoteka) {
		return apotekaRepository.save(apoteka);
	}
 
   
 

}
