package it.apoteka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.apoteka.model.Kupovina;
import it.apoteka.model.Lek;
import it.apoteka.repository.KupovinaRepository;
import it.apoteka.service.KupovinaService;
import it.apoteka.service.LekService;

@Service
public class JpaKupovinaServiceImpl implements KupovinaService {
	
	@Autowired
	private KupovinaRepository kupovinaRepository;
	@Autowired
	private LekService lekService;

	@Override
	public Kupovina kupi(Long lekId, Integer kolicina) {
		
		if (lekId == null) {
			throw new IllegalArgumentException("id leka ne moze biti null!");
		}
		
		Lek lek =  lekService.findOne(lekId);
		
		if (lek == null) {
			throw new IllegalArgumentException("Ne postoji lek sa zadatim id!");
		}
		
		if (lek.getKolicina() >= kolicina) {
			
			Kupovina kupovina = new Kupovina();
			
			kupovina.setLek(lek);
			kupovina.setKolicina(kolicina);
			
			
			lek.setKolicina(lek.getKolicina() - kolicina);
			
			kupovinaRepository.save(kupovina);
			lekService.save(lek);
			
			return kupovina;
			
			
		}
		
		return null;
	}

}
