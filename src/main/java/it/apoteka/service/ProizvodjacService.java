package it.apoteka.service;

import org.springframework.data.domain.Page;

import it.apoteka.model.Proizvodjac;



public interface ProizvodjacService {
	
	Page<Proizvodjac> findAll(int pageNum);
	Proizvodjac findOne(Long id);
	Proizvodjac save(Proizvodjac proizvodjac);
	void delete(Long id);
}
