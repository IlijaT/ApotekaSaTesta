package it.apoteka.service;

import org.springframework.data.domain.Page;

import it.apoteka.model.Apoteka;



public interface ApotekaService {
	
	Page<Apoteka> findAll(int pageNum);
	Apoteka findOne(Long id);
	Apoteka save(Apoteka apoteka);
	void delete(Long id);
}
