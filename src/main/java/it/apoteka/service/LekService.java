package it.apoteka.service;




import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import it.apoteka.model.Lek;



public interface LekService {
	
	Lek findOne(Long id);
	
	Page<Lek> findAll(int pageNum);
	
	Lek save(Lek Lek);

	void delete(Long id);
	
	Page<Lek> pretraga(
			@Param("naziv") String naziv, 
			@Param("generickiNaziv") String generickiNaziv, 
			@Param("apotekaId") Long apotekaId,
			int page);
	
//	Page<Lek> pretraga(
//	@Param("naziv") String naziv, 
//	@Param("apotekaId") Long apotekaId,
//	int page);


	Page<Lek> findByApotekaId(Long apotekaId, int page);
	
	
}
