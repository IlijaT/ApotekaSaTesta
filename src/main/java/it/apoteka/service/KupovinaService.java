package it.apoteka.service;

import it.apoteka.model.Kupovina;

public interface KupovinaService {
	
	Kupovina kupi (Long lekId, Integer kolicina);

}
