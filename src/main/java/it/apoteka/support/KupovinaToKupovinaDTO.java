package it.apoteka.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import it.apoteka.model.Kupovina;
import it.apoteka.web.dto.KupovinaDTO;

@Component
public class KupovinaToKupovinaDTO implements Converter<Kupovina, KupovinaDTO> {

	@Override
	public KupovinaDTO convert(Kupovina source) {
		KupovinaDTO dto = new KupovinaDTO();
		
		dto.setId(source.getId());
		dto.setLekId(source.getLek().getId());
		dto.setLekNaziv(source.getLek().getNaziv());
		dto.setKolicina(source.getKolicina());
		dto.setUkupnaCena(source.getKolicina() * source.getLek().getCena());
		
		
		return dto;
	}

}
