package it.apoteka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import it.apoteka.model.Proizvodjac;
import it.apoteka.web.dto.ProizvodjacDTO;


@Component
public class ProizvodjacToProizvodjacDTO 
	implements Converter<Proizvodjac, ProizvodjacDTO> {

	@Override
	public ProizvodjacDTO convert(Proizvodjac source) {
		
		ProizvodjacDTO dto = new ProizvodjacDTO();
		
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		 
		 
		
		return dto;
	}
	
	public List<ProizvodjacDTO> convert(List<Proizvodjac> iydavaci){
		List<ProizvodjacDTO> ret = new ArrayList<>();
		
		for(Proizvodjac x : iydavaci){
			ret.add(convert(x));
		}
		
		return ret;
	}

}
