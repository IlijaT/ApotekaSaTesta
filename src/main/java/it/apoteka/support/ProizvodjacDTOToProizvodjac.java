package it.apoteka.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import it.apoteka.model.Proizvodjac;
import it.apoteka.service.ProizvodjacService;
import it.apoteka.web.dto.ProizvodjacDTO;




@Component
public class ProizvodjacDTOToProizvodjac 
	implements Converter<ProizvodjacDTO, Proizvodjac>{
	
	@Autowired
	private ProizvodjacService proizvodjacService;
	
	
	@Override
	public Proizvodjac convert(ProizvodjacDTO dto) {
			Proizvodjac retVal = new Proizvodjac();
		
		if(dto.getId() != null) {
			retVal = proizvodjacService.findOne(dto.getId());
			if(retVal == null) {
				throw new IllegalStateException("tried to modify a non-existant proizvodjac");							
			}				
		}
		
		retVal.setId(dto.getId());
		retVal.setNaziv(dto.getNaziv());
			
		return retVal;
	}

}
