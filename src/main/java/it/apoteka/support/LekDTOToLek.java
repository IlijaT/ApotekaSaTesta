package it.apoteka.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import it.apoteka.model.Lek;
import it.apoteka.service.ApotekaService;
import it.apoteka.service.LekService;
import it.apoteka.service.ProizvodjacService;
import it.apoteka.web.dto.LekDTO;


@Component
public class LekDTOToLek 
	implements Converter<LekDTO, Lek>{
	
	@Autowired
	private LekService lekService;
	
	@Autowired
	private ApotekaService apotekaService;
	
	@Autowired
	private ProizvodjacService proizvodjacService;
	
	
	@Override
	public Lek convert(LekDTO dto) {
			Lek retVal = new Lek();
		
		if(dto.getId() != null) {
			retVal = lekService.findOne(dto.getId());
			
			if(retVal == null) {
				throw new IllegalStateException("tried to modify a non-existant lek");							
			}				
		}
		
		retVal.setId(dto.getId());
		retVal.setNaziv(dto.getNaziv());
		retVal.setKolicina(dto.getKolicina());
		retVal.setGenerickiNaziv(dto.getGenerickiNaziv());
		retVal.setCena(dto.getCena());
		retVal.setApoteka(apotekaService.findOne(dto.getApotekaId()));
		retVal.setProizvodjac(proizvodjacService.findOne(dto.getProizvodjacId()));
		
		
			
		return retVal;
	}

}
