//package it.apoteka.support;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.convert.converter.Converter;
//import org.springframework.stereotype.Component;
//
//import it.apoteka.model.Apoteka;
//import it.apoteka.service.ApotekaService;
//import it.apoteka.web.dto.ApotekaDTO;
//
//
//
//@Component
//public class ApotekaDTOToApoteka 
//	implements Converter<ApotekaDTO, Apoteka>{
//	
//	@Autowired
//	private ApotekaService apotekaService;
//	
//	
//	@Override
//	public Apoteka convert(ApotekaDTO dto) {
//			Apoteka retVal = new Apoteka();
//		
//		if(dto.getId() != null) {
//			retVal = apotekaService.findOne(dto.getId());
//			
//			if(retVal == null) {
//				throw new IllegalStateException("tried to modify a non-existant apoteka");							
//			}				
//		}
//		
//		retVal.setId(dto.getId());
//		retVal.setAdresa(dto.getAdresa());
//		retVal.setNaziv(dto.getNaziv());
//		
//		return retVal;
//	}
//
//}
