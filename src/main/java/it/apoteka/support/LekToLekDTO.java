package it.apoteka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import it.apoteka.model.Lek;
import it.apoteka.web.dto.LekDTO;


@Component
public class LekToLekDTO 
	implements Converter<Lek, LekDTO> {

	@Override
	public LekDTO convert(Lek lek) {
		
		LekDTO dto = new LekDTO();
		
		dto.setId(lek.getId());
		dto.setNaziv(lek.getNaziv());
		dto.setGenerickiNaziv(lek.getGenerickiNaziv());
		dto.setKolicina(lek.getKolicina());
		dto.setCena(lek.getCena());
		dto.setProizvodjacId(lek.getProizvodjac().getId());
		dto.setProizvodjacNaziv(lek.getProizvodjac().getNaziv());
		dto.setApotekaId(lek.getApoteka().getId());
		dto.setApotekaNaziv(lek.getApoteka().getNaziv());
		
		
		return dto;
	}
	
	public List<LekDTO> convert(List<Lek> lekovi){
		List<LekDTO> ret = new ArrayList<>();
		
		for(Lek x : lekovi){
			ret.add(convert(x));
		}
		
		return ret;
	}

}
