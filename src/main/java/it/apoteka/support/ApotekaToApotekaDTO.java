package it.apoteka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import it.apoteka.model.Apoteka;
import it.apoteka.web.dto.ApotekaDTO;


@Component
public class ApotekaToApotekaDTO 
	implements Converter<Apoteka, ApotekaDTO> {

	@Override
	public ApotekaDTO convert(Apoteka source) {
		
		ApotekaDTO dto = new ApotekaDTO();
		
		dto.setId(source.getId());
		dto.setAdresa(source.getAdresa());
		dto.setNaziv(source.getNaziv());
		return dto;
	}
	
	public List<ApotekaDTO> convert(List<Apoteka> apoteke){
		List<ApotekaDTO> ret = new ArrayList<>();
		
		for(Apoteka x : apoteke){
			ret.add(convert(x));
		}
		
		return ret;
	}

}
