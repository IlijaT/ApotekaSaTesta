package it.apoteka.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.apoteka.model.Kupovina;

@Repository
public interface KupovinaRepository extends JpaRepository<Kupovina, Long> {

}
