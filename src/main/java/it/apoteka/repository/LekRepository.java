package it.apoteka.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.apoteka.model.Lek;

@Repository
public interface LekRepository 
	extends JpaRepository<Lek, Long> {

	Page<Lek> findByApotekaId(Long apotekaId, Pageable pageRequest);

	@Query("SELECT l FROM Lek l WHERE "
			+ "(:apotekaId IS NULL OR l.apoteka.id = :apotekaId) AND"
			+ "(:naziv IS NULL or l.naziv LIKE CONCAT('%', :naziv, '%') or l.generickiNaziv LIKE CONCAT('%', :generickiNaziv, '%') )"
			)
	Page<Lek> pretraga(
			@Param("naziv") String naziv, 
			@Param("generickiNaziv") String generickiNaziv, 
			@Param("apotekaId") Long apotekaId,
			Pageable pageRequest);
	
//	@Query("SELECT l FROM Lek l WHERE "
//			+ "(:naziv IS NULL or l.naziv LIKE CONCAT('%', :naziv, '%') ) AND "
//			+ "(:naziv IS NULL or l.generickiNaziv LIKE CONCAT('%', :naziv, '%') ) AND "
//			+ "(:apotekaId IS NULL OR l.apoteka.id = :apotekaId)"
//			)
//	Page<Lek> pretraga(
//			@Param("naziv") String naziv, 
//			@Param("apotekaId") Long apotekaId,
//			Pageable pageRequest);

}
