package it.apoteka;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.apoteka.model.Apoteka;
import it.apoteka.model.Lek;
import it.apoteka.model.Proizvodjac;
import it.apoteka.service.ApotekaService;
import it.apoteka.service.LekService;
import it.apoteka.service.ProizvodjacService;



@Component
public class TestData {
	
	@Autowired
	private ProizvodjacService proizvodjacService;
	
	@Autowired
	private LekService lekService;
	
	@Autowired
	private ApotekaService apotekaService;
	
	@PostConstruct
	public void init(){ 
		
		Proizvodjac p1 = new Proizvodjac();
		p1.setNaziv("Hemofarm");
		proizvodjacService.save(p1);
		
		Proizvodjac p2 = new Proizvodjac();
		p2.setNaziv("Galenika");
		proizvodjacService.save(p2);
		
		Apoteka a1 = new Apoteka();
		a1.setNaziv("Mirkovic - Novi Sad");
		a1.setAdresa("Pariske Komune 31, Novi Sad");
		apotekaService.save(a1);
		
		Apoteka a2 = new Apoteka();
		a2.setNaziv("Mirkovic - Novi Kozarci");
		a2.setAdresa("Kralja Petra I 54 Novi Kozarci");
		apotekaService.save(a2);
		
		Apoteka a3 = new Apoteka();
		a3.setNaziv("Mirkovic - Kraljevci");
		a3.setAdresa("Rumska 22, Kraljevci");
		apotekaService.save(a3);
		
		Lek l1 = new Lek();
		l1.setNaziv("Presolol");
		l1.setGenerickiNaziv("Metoprolol");
		l1.setCena(700.0);
		l1.setKolicina(300);
		l1.setProizvodjac(p1);
		l1.setApoteka(a1);
		
		lekService.save(l1);
		
		
		Lek l2 = new Lek();
		l2.setNaziv("Paracetamol");
		l2.setGenerickiNaziv("Paracetamol");
		l2.setCena(530.0);
		l2.setKolicina(33);
		l2.setProizvodjac(p2);
		l2.setApoteka(a1);
		
		lekService.save(l2);
		
		Lek l3 = new Lek();
		l3.setNaziv("Apis Gola");
		l3.setGenerickiNaziv("Apis Gola");
		l3.setCena(1000.0);
		l3.setKolicina(3);
		l3.setProizvodjac(p1);
		l3.setApoteka(a2);
		
		lekService.save(l3);
		
		Lek l4 = new Lek();
		l4.setNaziv("Defrinol");
		l4.setGenerickiNaziv("Defrinol");
		l4.setCena(800.0);
		l4.setKolicina(13);
		l4.setProizvodjac(p2);
		l4.setApoteka(a3);
		
		lekService.save(l4);
		
		
		 
	}
	 
}
